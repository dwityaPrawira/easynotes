package com.dwitya.controller;

import com.dwitya.exception.ResourceNotFoundException;
import com.dwitya.model.Note;
import com.dwitya.repository.NoteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/api")
public class NoteController {

    Logger logger = LoggerFactory.getLogger(NoteController.class);

    @Autowired
    NoteRepository noteRepository;

    // get all notes
    @GetMapping("/notes")
    public List<Note> getAllNotes() {
        return noteRepository.findAll();
    }

    // save notes
    @PostMapping("/notes")
    public Note createNote(@Valid @RequestBody Note note) {
        try{
            logger.info(note.getTitle());
            logger.info(note.getContent());

            return noteRepository.save(note);
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }

    // get single notes
    @GetMapping("/notes/{id}")
    public Note getNoteById(@PathVariable(value = "id") Long noteId) {
        return noteRepository.findById(noteId).orElseThrow(() -> new ResourceNotFoundException("Note", "id", noteId));
    }

    // update a note
    @PutMapping("/notes/{id}")
    public Note updateNote(@PathVariable(value = "id") Long noteId, @Valid @RequestBody Note noteDetail) {
        Note note = noteRepository.findById(noteId).orElseThrow(() -> new ResourceNotFoundException("Note", "id", noteId));
        note.setTitle(noteDetail.getTitle());
        note.setContent(noteDetail.getContent());

        Note updatedNote = noteRepository.save(note);
        return updatedNote;
    }

    // delete a note
    @DeleteMapping("/notes/{id}")
    public ResponseEntity<?> deleteNote(@PathVariable(value = "id") Long noteId) {
        Note note = noteRepository.findById(noteId).orElseThrow(() -> new ResourceNotFoundException("Note", "id", noteId));
        noteRepository.delete(note);
        return ResponseEntity.ok().build();
    }

}
